package me.jonakls.simpleautorespawn;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class SimpleAutoRespawn extends JavaPlugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new PlayerDeathListener(), this);
    }
}

class PlayerDeathListener implements Listener {

    @EventHandler
    private void onDeath(PlayerDeathEvent event) {
        event.getEntity().spigot().respawn();
    }
}


